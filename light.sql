-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 16 2018 г., 23:01
-- Версия сервера: 5.5.53
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `light`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `depth` varchar(100) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `message_id`, `content`, `user_id`, `parent`, `depth`, `time`) VALUES
(89, 39, 'Yeah!', 4, 0, '1', '2018-03-16 17:23:36'),
(99, 39, 'Yeah!', 17, 89, '1.4', '2018-03-16 18:38:52'),
(101, 41, 'What?', 4, 0, '1', '2018-03-16 20:13:52'),
(102, 91, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco!', 17, 0, '1', '2018-03-16 20:18:24'),
(103, 91, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit!', 17, 102, '1.2', '2018-03-16 20:18:39'),
(104, 103, 'dassda', 17, 0, '1', '2018-03-16 20:28:29'),
(107, 102, 'Таким образом рамки и место обучения кадров позволяет оценить значение дальнейших направлений развития. Идейные соображения высшего порядка, а также рамки и место обучения кадров влечет за собой процесс внедрения и модернизации системы обучения кадров, со', 17, 0, '1', '2018-03-16 20:36:33'),
(108, 102, 'Таким образом рамки и место обучения кадров позволяет оценить значение дальнейших направлений развития. Идейные соображения высшего порядка, а также рамки и место обучения кадров влечет за собой процесс внедрения и модернизации системы обучения кадров, со', 17, 0, '2', '2018-03-16 20:36:38'),
(109, 39, 'No!', 4, 99, '1.4.2', '2018-03-16 20:38:40'),
(114, 39, '(x_x)', 4, 89, '1.5', '2018-03-16 19:42:19');

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `content` varchar(300) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `message`
--

INSERT INTO `message` (`id`, `content`, `sender_id`, `time`) VALUES
(39, 'For the glory!', 1, '2018-03-08 14:44:04'),
(41, 'Nice QA', 1, '2018-03-08 15:06:13'),
(89, 'Ennio!', 4, '2018-03-16 20:12:32'),
(91, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cill', 17, '2018-03-16 20:17:10'),
(97, 'Идейные соображения высшего порядка, а также укрепление и развитие структуры представляет собой интересный эксперимент проверки соответствующий условий активизации. Задача организации, в особенности же консультация с широким активом играет важную роль в формировании существенных финансовых и админис', 17, '2018-03-16 20:20:43'),
(98, 'Разнообразный и богатый опыт постоянное информационно-пропагандистское обеспечение нашей деятельности играет важную роль в формировании систем массового участия. Таким образом сложившаяся структура организации влечет за собой процесс внедрения и модернизации модели развития.', 17, '2018-03-16 20:21:05'),
(99, 'С другой стороны новая модель организационной деятельности требуют определения и уточнения дальнейших направлений развития. Разнообразный и богатый опыт реализация намеченных плановых заданий играет важную роль в формировании направлений прогрессивного развития. Идейные соображения высшего порядка, ', 17, '2018-03-16 20:21:22'),
(101, 'Не следует, однако забывать, что реализация намеченных плановых заданий требуют определения и уточнения системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что консультация с широким активом позволяет выполнять важные задания по разработке систем массового учас', 17, '2018-03-16 20:24:46'),
(102, 'Таким образом рамки и место обучения кадров позволяет оценить значение дальнейших направлений развития. Идейные соображения высшего порядка, а также рамки и место обучения кадров влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям. Не следует', 17, '2018-03-16 20:27:34');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(30) NOT NULL DEFAULT 'Аноним',
  `last_name` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(30) NOT NULL,
  `avatar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `avatar`) VALUES
(1, 'василий', 'петров', 'admin@gmail.com', ''),
(4, 'максим', 'снигур', 'darksaid98@gmail.com', 'https://lh6.googleusercontent.com/-UoAtiEsc6xo/AAAAAAAAAAI/AAAAAAAAABI/iYMiw0Hj5y4/photo.jpg'),
(17, 'Anonimus', '', 'developorange@gmail.com', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `message_id` (`message_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender_id` (`sender_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT для таблицы `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
