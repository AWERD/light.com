<div class="container">
	<div class="container__center">
		<h1>Добро пожаловать!</h1>
		<p>Авторизируйтесь с помощью Google.</p>
		<button class="btn btn-danger" onclick="window.location = '<?=  $data['loginUrl']; session_unset(); ?>'">Google +</button>
	</div>
	<div class="container__hello">
		<img src="/core/web/img/hello.png" alt="" class="hello__img">
	</div>
</div>