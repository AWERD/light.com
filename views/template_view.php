<!DOCTYPE html>
<html lang="ru">
<head>
	<link rel="stylesheet" type="text/css" href="/core/web/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/core/web/css/custom.css">
	<link rel="stylesheet" type="text/css" href="/core/web/css/nprogress.css">
	<link href="/core/web/css/fontawesome-all.min.css" rel="stylesheet">
	<script src="/core/web/js/jquery-3.3.1.min.js"></script>
	<script src="/core/web/js/nprogress.js"></script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Главная</title>
</head>
<body>
	<?php include PATH_ROOT.'/views/'.$content_view; ?>
	<footer class="footer">
		<div class="footer__logout"><a target="_blank" href="http://cybueh09.ho.ua/">Тут ничего нет.</a></div>
	</footer>
</body>
</html>