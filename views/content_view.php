<div class="container">
	<?php
	if($data['auth']) :
	?>
		<div class="logout"><a href="/main/logout">Выход <i class="fas fa-power-off"></i></a></div>
		<h1>Оставьте своё сообщение!</h1>
		<div class="form-group">
		  <!-- <label for="comment">Текст:</label> -->
		  <textarea class="form-control" rows="4" id="send_message_text"></textarea>
		</div>
		<button id="send_message" class="btn btn-success">Отправить</button>
	<?php else : ?>
		<h1><a href="/">Авторизируйтесь</a>, что бы оставить сообщение.</h1>
	<?php endif; ?>
	<div class="messages-wrapper" id="messages-wrapper">
	</div>
</div>
<script src="/core/web/js/custom.js"></script>