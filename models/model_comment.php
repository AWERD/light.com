<?php 

class Model_Comment extends Model
{
	/**
	 * Get all comment from database
	 * @return [array] [array with reults]
	 */
	public function get_data()
	{	
		return $this->db->query('SELECT * FROM comment');
	}

	/**
	 * Insert parent comment to database 
	 * @param  [int] $reply_id   [id of message to reply]
	 * @param  [string] $message    [text of message]
	 * @param  [integer] $id         [current user id]
	 * @param  [integer] $message_id [id of message]
	 */
	public function save_comment($reply_id, $message, $id, $message_id)
	{
		$count = $this->db->query('SELECT COUNT(id) AS count FROM comment WHERE (comment.parent = 0 AND message_id = ' . $message_id . ')');
		$depth = $count[0]['count'] + 3;
		return $this->db->query('INSERT INTO comment(message_id, content, depth, user_id, time) VALUES ('. $reply_id .',"'. $message .'","'. $depth .'", '. $id .', "'. date('Y-m-d G:i:s') .'")');
	}

	/**
	 * Insert child comment to database
	 * @param  [int] $reply_id   [id of message to reply]
	 * @param  [string] $message    [text of message]
	 * @param  [integer] $id         [current user id]
	 * @param  [integer] $message_id [id of comment to reply]
	 */
	public function save_comment_reply($reply_id, $message, $id, $message_id)
	{
		$count = $this->db->query('SELECT COUNT(id) AS count, comment.depth FROM comment WHERE (comment.parent = ' . $reply_id . ' AND message_id = ' . $message_id . ')');
		$depth = $this->db->query('SELECT depth AS count, comment.depth FROM comment WHERE (id = ' . $reply_id . ' AND message_id = ' . $message_id . ')');
		$depth = $depth[0]['depth'] . '.' . ($count[0]['count'] + 3);
		return $this->db->query('INSERT INTO comment(message_id, parent, content, depth,  user_id, time) VALUES ('. $message_id .','. $reply_id .',"'. $message .'","'. $depth .'", '. $id .', "'. date('Y-m-d G:i:s') .'")');
	}

	/**
	 * Get all message comments sort by depth in a tree
	 * @param  [integer] $id [id of message]
	 */
	public function get_comments($id) 
	{
		return $this->db->query('SELECT comment.id AS comment, comment.message_id, comment.content, comment.depth, comment.time, users.id, users.first_name, users.last_name, users.avatar FROM comment 
			INNER JOIN users ON comment.user_id = users.id
			INNER JOIN message ON comment.message_id = message.id
			WHERE message.id = ' . $id . ' ORDER BY comment.depth');
	}

	/**
	 * Delete current comment drim database
	 * @param  [integer] $id [id of comment]
	 */
	public function delete_comment($id)
	{
		return $this->db->query('DELETE FROM comment WHERE (id = ' . $id .')');
	}
}

?>