<?php 

class Model_Message extends Model
{
	/**
	 * Get all messages from database
	 * @return [array] [array with reults]
	 */
	public function get_data()
	{	
		return $this->db->query('SELECT * FROM message');
	}

	/**
	 * Insert message in database
	 * @param  [string] $message [text of message]
	 * @param  [integer] $id      [user id]
	 */
	public function save_message($message, $id)
	{
		return $this->db->query('INSERT INTO message(content, sender_id, time) VALUES ("'. $message .'", '. $id .', "'. date('Y-m-d G:i:s') .'")');
	}

	/**
	 * Get all messages from database with limits (for infinity scroll)
	 * @param  [integer] $limit  [limit of rows]
	 * @param  [integer] $offset [start position]
	 * @return [type]         [description]
	 */
	public function get_messages($limit, $offset) 
	{
		return $this->db->query("SELECT message.content, message.time, message.id, users.first_name, users.last_name, users.email, users.avatar, users.id AS auth
			FROM message
			LEFT JOIN users ON message.sender_id = users.id ORDER BY message.id DESC LIMIT " . $offset . ", " . $limit);
	}

	/**
	 * Delete message from database
	 * @param  [integer] $id [message id]
	 * @return [type]     [description]
	 */
	public function delete_message($id)
	{
		return $this->db->query('DELETE FROM message WHERE (id = ' . $id .')');
	}
}

?>