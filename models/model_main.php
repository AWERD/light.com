<?php 
class Model_Main extends Model
{
	/**
	 * Get all users from database
	 * @return [array] [array with reults]
	 */
	public function get_data()
	{	
		$db = new DB();
		return $db->query('SELECT * FROM users');
	}

	/**
	 * Validate data with google auth
	 */
	public function validate_data()
	{
		$db = new DB();
		require_once(PATH_ROOT.'/core/libs/GoogleAPI/config.php');

		if(isset($_SESSION['access_token']))
		{
			$gClient->setAccessToken($_SESSION['access_token']);
		}
		else if(isset($_GET['code']))
		{
			$token = $gClient->fetchAccessTokenWithAuthCode($_GET['code']);
			$_SESSION['access_token'] = $token;
		}
		else 
		{
			return false;
		}

		$oAuth = new Google_Service_Oauth2($gClient);
		$userData = $oAuth->userinfo_v2_me->get();

		if($userData) 
		{
			$_SESSION['email'] = $userData['email'];
			$_SESSION['picture'] = $userData['picture'];
			$_SESSION['lastName'] = $userData['familyName'];
			$_SESSION['firstName'] = $userData['givenName'];
			$user = $db->query('SELECT id FROM users WHERE email = "'. $userData['email'] . '"');
			if(count($user) == 0)
			{
				$userData['givenName'] ? $name = $userData['givenName'] : $name = 'Anonimus';
				$userData['familyName'] ? $lastname = $userData['familyName'] : $lastname = '';
				$insert = $db->query("INSERT INTO users(email, first_name,  last_name, avatar) VALUES('".$userData['email']."', '". $name ."', '". $lastname ."','".$userData['picture']."');");
				$_SESSION['id'] = $db->query('select last_insert_id();');
				return true;
			}
			else
			{
				$_SESSION['id'] = $user[0]['id'];
			}
			return true;
		}
		return false;
	}
}

?>