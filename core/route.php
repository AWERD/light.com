<?php 

class Route
{
	/**
	 * Route to controller's action
	 */
	static function start()
	{
		
		session_start();
		// default controller and action
		$controller_name = 'Main';
		$action_name = 'index';
		$routes = explode('?', $_SERVER['REQUEST_URI']);
		$get = $routes[1];
		$routes = explode('/', $routes[0]);
		// get controller name
		if ( !empty($routes[1]) )
		{	
			$controller_name = $routes[1];
		}
		
		// get action name
		if ( !empty($routes[2]) )
		{
			$action_name = $routes[2];
		}

		// add prefix
		$model_name = 'Model_'.$controller_name;
		$controller_name = 'Controller_'.$controller_name;
		$action_name = 'action_'.$action_name;

		// include file with model class
		$model_file = strtolower($model_name).'.php';
		$model_path = "../models/".$model_file;
		if(file_exists($model_path))
		{
			include "../models/".$model_file;
		}

		// include file with contrlooer class
		$controller_file = strtolower($controller_name).'.php';
		$controller_path = "controllers/".$controller_file;
		if(file_exists($controller_path))
		{
			require_once(PATH_ROOT."/controllers/".$controller_file);
		}
		else
		{
			// redirect to 404
			Route::ErrorPage404();
		}
		
		// create controller
		$controller = new $controller_name;
		$action = $action_name;
		
		if(method_exists($controller, $action))
		{
			// call controller action
			$controller->$action();
		}
		else
		{
			Route::ErrorPage404();
		}
	
	}
	
	function ErrorPage404()
	{
  //       $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
  //       header('HTTP/1.1 404 Not Found');
		// header("Status: 404 Not Found");
		// header('Location:'.$host.'404');
    }
}

?>