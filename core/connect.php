<?php
class DB 
{
	// set connection data
	protected $db_name = 'light';
	protected $db_user = 'root';
	protected $db_pass = '';
	protected $db_host = 'localhost';
	public $db_connection = '';

	/**
	 * Create database connection
	 */
	function __construct()
	{
		$this->db_connection = new mysqli( $this->db_host, $this->db_user, $this->db_pass, $this->db_name );
		if ( mysqli_connect_errno() ) {
			printf("Connection failed: %s", mysqli_connect_error());
			exit();
		}
	}
	
	/**
	 * Execute query
	 * @param  [string] $query [query to execute]
	 * @return [string]        [return data from query]
	 */
	public function query($query)
	{
		$result = [];
		$rows = $this->db_connection->query($query);
		while($row = mysqli_fetch_assoc($rows))
		{
			array_push($result, $row);
		}
		return $result;
	}
}
?>