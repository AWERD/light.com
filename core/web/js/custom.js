jQuery(document).ready(function($) {
	var cuurent_comment_id = 0;
	var limit = 5;
	var offset = 0;
	var auth = false;

	$(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() == $(document).height()) {
	       offset += 4;
	       getAllMessages();
	   }
	});

	$('#messages-wrapper').innerHTML = '';
	getAllMessages();

	$(document).on('click', '.comment', showComments);
	$(document).on('click', '.reply-comment', setReply);
	$(document).on('click', '.reply-btn', function(){
		var content = $(this).prev();
		var type = $(this).data('type');
		var reply = $(this).data('id');
		var message_id = $(this).data('message_id');
		sendComment(content.val(), reply, type, message_id);
	});

	$(document).on('click', '.item-comment__delete', function(){
		var id = $(this).data('id');
		var message_id = $(this).data('message_id');
		deleteComment(id, message_id);
		$(this).parent().remove();
	})

	$(document).on('click', '.item-message__delete', function(){
		var id = $(this).data('id');
		var message_id = $(this).data('message_id');
		deleteMessage(id, message_id);
		$(this).parent().parent().remove();
	})

	$('#send_message').click(function(event) {
		var content = $('#send_message_text').val();
		if(content !== '')
		{
			sendMessage(content);
			$(this).val('');
		}
	});



	function getAllMessages() 
	{
		NProgress.start();
		$.ajax({
			type: "POST",
			url: '/content/getmessages',
			data:  {
				limit: limit,
				offset: offset,
			},
    		dataType:'JSON', 
			success: function ( response )
			{
				if(response && response !== '')
				{	
					sessionStorage.setItem('id', response[0]['id']);
					$('#messages-wrapper').append(messages_template(response[1]));
					NProgress.done();
				}
			},
			error: function( response )
			{
				alert('Ошибка при загрузке сообщений.');
			},
		});
	}

	function updateAllMessages() 
	{
		NProgress.start();
		$.ajax({
			type: "POST",
			url: '/content/getmessages',
			data:  {
				limit: limit,
				offset: 0,
			},
    		dataType:'JSON', 
			success: function ( response )
			{
				if(response && response !== '')
				{	
					sessionStorage.setItem('id', response[0]['id']);
					$('#messages-wrapper').html(messages_template(response[1]));
					NProgress.done();
				}
			},
			error: function( response )
			{
				alert('Ошибка при загрузке сообщений.');
			},
		});
	}

	function setReply()
	{
		var message = $(this).data('message-id');
		var comment = $(this).data('comment-id');
		var message_id = $(this).data('message');
		var nerest = this.closest('.item');
		if( !$(this).hasClass('active') ){
			var input = document.createElement("input");
			var btn = document.createElement("button");
			input.type = "text";
			input.className = "reply-input"; 
			btn.innerHTML = 'Отправить';
			btn.className = "reply-btn ";
			btn.dataset.type = message ? 'message' : 'comment'
			btn.dataset.id = message ? message : comment;
			btn.dataset.message_id = message ? message : message_id;
			nerest.appendChild(input); 
			nerest.appendChild(btn); 
			$(this).addClass('active');
		} else {
			$(nerest).children('.reply-input').remove();
			$(nerest).children('.reply-btn').remove();
			$(this).removeClass('active');
		}
	}

	/**
	 * Comments ajax
	 */

	function showComments()
	{
		var id = $(this).data('message-id');
		var el = $(this);
		if(el.html() == 'Показать коментарии' && cuurent_comment_id !== id) 
		{
			$('#comments-wrapper-'+id).css('display', 'block');
			udpateComment(id);
			el.html('Скрыть');
		} 
		else if(el.html() == 'Показать коментарии' && cuurent_comment_id == id)
		{
			el.html('Скрыть')
			$('#comments-wrapper-'+id).css('display', 'block');
		}
		else
		{
			el.html('Показать коментарии')
			$('#comments-wrapper-'+id).css('display', 'none');
		}
			
	}

	function udpateComment(id) {
		NProgress.start();
		$.ajax({
			url: '/comment/getcomments',
			type: "POST",
			dataType: 'json',
			data: {
				id: id,
			},
			success: function(response) {	
				NProgress.done();
				if(response.length > 0)
				{
					cuurent_comment_id = id;
					$("#comments-wrapper-" + id).html(comments_template(response, id));
				}
			},
			error: function(response) {
			}
		});
	}

	function sendComment(content, reply_id = 0, type = 'message', message_id = 0) {
		$.ajax({
			type: "POST",
			url: '/content/savemessage',
			data: {
				message: content,
				reply: reply_id,
				id: sessionStorage.getItem('id'),
				type: type,
				message_id: message_id,
			},
    		dataType:'JSON', 
			success: function ( response )
			{
				if(response && response !== '' && response.length > 0)
				{
					$('#send_message_text').val('');
					updateAllMessages();
				}
			},
			error: function( response )
			{
				udpateComment(message_id);
			},
		});
	}

	function deleteComment(id, message_id) {
		$.ajax({
			type: "POST",
			url: '/content/deletecomment',
			data: {
				id: id,
			},
    		dataType:'JSON', 
			success: function ( response )
			{
				if(response && response !== '' && response.length > 0)
				{
					udpateComment(message_id);
				}
			},
			error: function( response )
			{
				udpateComment(message_id);
			},
		});
	}


	/**
	 * Message functions
	 */	
	
	function sendMessage(content, reply_id = 0, type = 'message', message_id = 0) {
		$.ajax({
			type: "POST",
			url: '/content/savemessage',
			data: {
				message: content,
				reply: reply_id,
				id: sessionStorage.getItem('id'),
				type: type,
				message_id: message_id,
			},
    		dataType:'JSON', 
			success: function ( response )
			{
				if(response && response !== '' && response.length > 0)
				{
					$('#send_message_text').val('');
					updateAllMessages();
				}
			},
			error: function( response )
			{
				updateAllMessages();
			},
		});
	}

 	function deleteMessage(id, message_id) {
 		$.ajax({
 			type: "POST",
 			url: '/content/deleteMessage',
 			data: {
 				id: id,
 			},
     		dataType:'JSON', 
 			success: function ( response )
 			{
 				if(response && response !== '' && response.length > 0)
 				{
 					getAllMessages();
 				}
 			},
 			error: function( response )
 			{
 				return true;
 			},
 		});
 	}


	/**
	 * Templates
	 */

	function messages_template(response, id) 
	{
		var template ='';
		response.forEach( function(element, index) {
			var img = '/core/web/img/default.png';
			var reply = '';
			var delete_message = '';
			if( element.auth == sessionStorage.getItem('id') ){
				delete_message = '<div class="item-message__delete" data-message_id=' + element.id + ' data-id=' + element.id + '><i class="fas fa-times"></i></div>';
			}
			if( sessionStorage.getItem('id') !== 'false' )
			{
				reply = '<div class="reply"><small>'+
							 '<a class="reply-comment" onclick="return false" data-message-id="' + element.id + '" href="">Ответить</a>'+
						'</small></div>';
			}
			if( element.avatar )
			{
				img = element.avatar;
			}
			template += '<div class="item-message" style="position: relative;"><hr><div class="item">'+
							'<div class="account-details">'+
								'<img src="' + img + '">'+
								'<div class="name lead">'+
									element.first_name + ' ' + element.last_name +
								'</div>'+
							'</div>'+
							'<div class="message-content">'+
								'<div class="message">'+
									element.content +
								'</div>'+
								'<div class="d-flex">'+
									'<div class="time"><small>'+
										element.time +
									'</small></div>'+
									'<div class="reply"><small>'+
										 '<a class="comment" onclick="return false" data-message-id="' + element.id + '" href="">Показать коментарии</a>'+
									'</small></div>'+			
									reply +
								'</div>'+
							'</div>'+
							delete_message +
						'</div>'+
						'<div id="comments-wrapper-' + element.id + '" class="comments-wrapper"></div></div>';
		});
		return template;
	}





	function comments_template(response, id)
	{
		var template = '';
		response.forEach( function(element, index) {
			var depth = element.depth.split('.');
			var delete_comment = '';
			var img = '/core/web/img/default.png';
			var reply = '';
			if( element.id == sessionStorage.getItem('id') ){
				delete_comment = '<div class="item-comment__delete" data-message_id=' + element.message_id + ' data-id=' + element.comment + '><i class="fas fa-times"></i></div>';
			}
			if( sessionStorage.getItem('id') !== 'false' )
			{
				reply = '<div class="reply"><small>'+
							 '<a class="reply-comment" onclick="return false"data-message = "' + element.message_id + '" data-comment-id="' + element.comment + '" href="">Ответить</a>'+
						'</small></div>';
			}
			if(element.avatar)
				img = element.avatar;
			template += '<hr style="margin-left:' + 20 * depth.length + 'px"><div style="position: relative;margin-left:' + 20 * depth.length + 'px" class="item item-comment">'+
							'<div class="account-details">'+
								'<img src="' + img + '">'+
								'<div class="name lead">'+
									element.first_name + ' ' + element.last_name +
								'</div>'+
							'</div>'+
							'<div class="message-content">'+
								'<div class="message">'+
									element.content +
								'</div>'+
								'<div class="d-flex">'+
									'<div class="time"><small>'+
										element.time +
									'</small></div>'+
									reply +
								'</div>'+
							'</div>'+
							delete_comment +
						'</div>'+
						'<div id="comments-wrapper-' + element.id + '" class="comments-wrapper"></div>';
		});
		return template;
	}
});