<?php 

require_once(PATH_ROOT.'/models/model_message.php');
require_once(PATH_ROOT.'/models/model_main.php');
require_once(PATH_ROOT.'/models/model_comment.php');

class Controller_Content extends Controller
{

	function __construct()
	{
		$this->model = new Model_Message();
		$this->model_comment = new Model_Comment();
		$this->model_main = new Model_Main();
		$this->view = new View();
		$this->app = new App();
	}

	/**
	 * Default action
	 */
	function action_index()
	{	
		$this->model_main->validate_data();
		$data['users'] = $this->model->get_data();
		$_SESSION['access_token'] == null ? $data['auth'] = false : $data['auth'] = true;
		$this->view->render('content_view.php', 'template_view.php', $data);
	}

	/**
	 * Save messages and comments
	 */
	function action_savemessage() 
	{
		if($_POST['type'] == 'message' && $_POST['reply'] == 0)
		{
			if($this->model->save_message($_POST['message'], $_POST['id']))
			{
				echo json_encode('true');
			}
		}
		else if($_POST['type'] == 'comment')
		{
			if($this->model_comment->save_comment_reply($_POST['reply'], $_POST['message'], $_POST['id'], $_POST['message_id']))
			{
				echo json_encode('true');
			}
		}
		else if($_POST['type'] == 'message' && $_POST['reply'] !== 0)
		{
			if( $this->model_comment->save_comment($_POST['reply'], $_POST['message'], $_POST['id'], $_POST['message_id']) )
			{
				echo json_encode('true');
			}
		}
		else 
		{
			echo json_encode('false');
		}
	}

	/**
	 * Delete comment
	 */
	function action_deletecomment()
	{
		if( $this->model_comment->delete_comment($_POST['id']) )
		{
			echo json_encode('true');
		}
	}

	/**
	 * Delete message
	 * @return [type] [description]
	 */
	function action_deletemessage()
	{
		if( $this->model->delete_message($_POST['id']) )
		{
			echo json_encode('true');
		}
	}

	/**
	 * Get all messages from database
	 * @return [array] [array with all messagaes]
	 */
	function action_getmessages()
	{
		$responce = array();
		$user = array('id' => $this->app->getId());
		array_push($responce, $user);
		array_push($responce, $this->model->get_messages($_POST['limit'], $_POST['offset']));
		echo json_encode($responce);
	}
}

?>