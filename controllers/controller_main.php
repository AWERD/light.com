<?php 

require_once(PATH_ROOT.'/models/model_main.php');

class Controller_Main extends Controller
{

	function __construct()
	{
		$this->model = new Model_Main();
		$this->view = new View();
	}

	function action_index()
	{	
		if(!isset($_SESSION['access_token']))
		{
			require_once(PATH_ROOT.'/core/libs/GoogleAPI/config.php'); 
			$data['loginUrl'] = $gClient->createAuthUrl();
			$data['model'] = $this->model->get_data();
			$this->view->render('main_view.php', 'template_view.php', $data);
		}
		else 
		{
			?>
			<script type="text/javascript">
			window.location.href = 'http://light.com/content';
			</script>
			<?php
		}
	}

	function action_google()
	{
		if($this->model->validate_data())
		{
			?>
			<script type="text/javascript">
			window.location.href = 'http://light.com/content';
			</script>
			<?php
		}
		else 
		{
			?>
			<script type="text/javascript">
			window.location.href = 'http://light.com';
			</script>
			<?php
		}

	}

	function action_logout()
	{
		session_destroy();
		?>
		<script type="text/javascript">
		window.location.href = 'http://light.com';
		</script>
		<?php
	}
}

?>