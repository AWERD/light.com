<?php 

require_once(PATH_ROOT.'/models/model_comment.php');

class Controller_Comment extends Controller
{

	function __construct()
	{
		$this->model = new Model_Comment();
		$this->view = new View();
	}

	function action_index()
	{	
	}

	/**
	 * Get current message comments 
	 */
	function action_getcomments() 
	{
		if($_POST['id']) 
		{
			echo json_encode($this->model->get_comments($_POST['id']));
		}
		else 
		{
			echo json_encode('Error');
		}
	}

	/**
	 * Display all messages
	 */
	function action_getmessages()
	{
		echo json_encode($this->model->get_messages());
	}
}

?>